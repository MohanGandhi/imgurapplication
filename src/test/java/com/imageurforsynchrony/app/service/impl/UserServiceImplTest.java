package com.imageurforsynchrony.app.service.impl;

import com.imageurforsynchrony.app.constants.ApplicationConstants;
import com.imageurforsynchrony.app.model.User;
import com.imageurforsynchrony.app.repository.UserRepository;
import com.imageurforsynchrony.app.request.UserRequest;
import com.imageurforsynchrony.app.response.ApiResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    private UserRequest userRequest;
    private User user;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        userRequest = new UserRequest("John","Doe");
        user = new User("John","Doe");
    }

    /***
     * This test method uses to test the register with fresh record
     */
    @Test
    public void registerTestWithSuccess(){
        Mockito.when(userRepository.findByUsername(Mockito.anyString())).thenReturn(null);
        user.setId(2);
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
        ApiResponse<User> apiResponse = userService.register(userRequest);
        Assert.assertTrue(apiResponse != null );
        Assert.assertEquals("Create user test", ApplicationConstants.USER_CREATION_SUCCESS_MESSAGE,apiResponse.getMessage());
    }

    /***
     * This test method uses to test the register with existing user
     */
    @Test
    public void registerTestWithExistingUser(){
        user.setId(2);
        Mockito.when(userRepository.findByUsername(Mockito.anyString())).thenReturn(user);
        ApiResponse<User> apiResponse = userService.register(userRequest);
        Assert.assertTrue(apiResponse != null );
        Assert.assertEquals("Existed user Test", ApplicationConstants.USER_ALREADY_EXIST,apiResponse.getMessage());
    }

    /***
     * This test method uses to test the getUsers without data
     */
    @Test
    public void getUsersTestWithoutData(){
        ApiResponse<List<User>> apiResponse = userService.getUsers();
        Assert.assertTrue(apiResponse != null );
        Assert.assertEquals("Empty users Test", ApplicationConstants.EMPTY_USERS,apiResponse.getMessage());
    }


    /***
     * This test method uses to test the getUsers with data
     */
    @Test
    public void getUsersTestWithData(){
        user.setId(2);
        List<User> users = new ArrayList<>();users.add(user);
        Mockito.when(userRepository.findAll()).thenReturn(users);
        ApiResponse<List<User>> apiResponse = userService.getUsers();
        Assert.assertTrue(apiResponse != null );
        Assert.assertTrue(apiResponse.getResult().size() > 0);
        Assert.assertEquals("Empty users Test", ApplicationConstants.ALL_USERS,apiResponse.getMessage());
    }

}
