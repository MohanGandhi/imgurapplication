package com.imageurforsynchrony.app.service.impl;

import com.imageurforsynchrony.app.config.ImgURConfig;
import com.imageurforsynchrony.app.constants.ApplicationConstants;
import com.imageurforsynchrony.app.helper.ImgUrResponseObject;
import com.imageurforsynchrony.app.model.ImgUrResponse;
import com.imageurforsynchrony.app.repository.ImgUrRepository;
import com.imageurforsynchrony.app.response.ApiResponse;
import com.imageurforsynchrony.app.utility.CustomResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpMethod;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;

public class ImageUploadServiceImplTest {

    @InjectMocks
    private ImageUploadServiceImpl imageUploadService;

    @Mock
    private ImgUrRepository imgUrRepository;

    @Mock
    private ImgURConfig imgURConfig;

    private CloseableHttpClient httpClient;

    private static final String IMAGE_ID = "TESTIMGID";

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(imgURConfig, "clientId", "QWERTYUI32cdf");
        ReflectionTestUtils.setField(imgURConfig, "baseurl", "http://imgur/image");
        this.httpClient = mock(CloseableHttpClient.class);


    }

    /**
     * This method uses to test the image from DB by imageID
     */
    @Test
    public void getImageFromDBTest(){
        ImgUrResponse imgUrResponse = new ImgUrResponse(IMAGE_ID,"http://imgur.com/"+IMAGE_ID,"png","QWERTASDF","JohnDoe");
        Mockito.when(imgUrRepository.findByImgUrId(Mockito.anyString())).thenReturn(imgUrResponse);
        ImgUrResponseObject imgUrResponseObject = imageUploadService.getImageFromDB("TESTIMGID");
        Assert.assertTrue(imgUrResponseObject != null );
        Assert.assertTrue(imgUrResponseObject.isSuccess());
        Assert.assertEquals("Get Image details by id from DB", IMAGE_ID,imgUrResponseObject.getId());
    }


    /**
     * This method uses to test the image details associated to the valid username
     */
    @Test
    public void findImagesByValidUsernameTest(){
        ImgUrResponse imgUrResponse = new ImgUrResponse(IMAGE_ID,"http://imgur.com/"+IMAGE_ID,"png","QWERTASDF","JohnDoe");
        List<ImgUrResponse> imgUrResponses = new ArrayList<>();
        imgUrResponses.add(imgUrResponse);
        Mockito.when(imgUrRepository.findAllByUsername(Mockito.anyString())).thenReturn(imgUrResponses);
        ApiResponse<List<ImgUrResponseObject>> imageDetails = imageUploadService.findImagesByUsername("TESTIMGID");
        Assert.assertTrue(imageDetails != null );
        Assert.assertTrue(imageDetails.getResult() != null);

        Assert.assertEquals("Get Image details associated to the given user from DB", ApplicationConstants.SUCCESS,imageDetails.getStatus());
    }

    /**
     * This method uses to test the image details associated to the invalid username
     */
    @Test
    public void findImagesByInValidUsernameTest(){
        List<ImgUrResponse> imgUrResponses = new ArrayList<>();
        Mockito.when(imgUrRepository.findAllByUsername(Mockito.anyString())).thenReturn(imgUrResponses);
        ApiResponse<List<ImgUrResponseObject>> imageDetails = imageUploadService.findImagesByUsername("TESTIMGID");
        Assert.assertTrue(imageDetails != null );
        Assert.assertTrue(imageDetails.getResult() == null);
        Assert.assertEquals("Get Image details associated to the given user from DB", ApplicationConstants.FAILED,imageDetails.getStatus());
    }

    @Test
    public void deleteImageTest(){
        ImgUrResponse imgUrResponse = new ImgUrResponse(IMAGE_ID,"http://imgur.com/"+IMAGE_ID,"png","QWERTASDF","JohnDoe");
        Mockito.when(imgUrRepository.findByImageHash(Mockito.anyString())).thenReturn(imgUrResponse);
        Mockito.doNothing().when(imgUrRepository).delete(Mockito.any());
        CustomResponseHandler customResponseHandler = new CustomResponseHandler(HttpMethod.DELETE);
        try {
            ImgUrResponseObject imgUrResponseObject = new ImgUrResponseObject();
//            Mockito.when(httpClient.execute(Mockito.any(HttpDelete.class),customResponseHandler)).thenReturn(imgUrResponseObject);
            Mockito.when(httpClient.execute(Mockito.any(HttpDelete.class))).thenReturn(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String result = imageUploadService.deleteImage(IMAGE_ID);
        Assert.assertTrue(result != null );
    }

}
