package com.imageurforsynchrony.app.controller;

import com.imageurforsynchrony.app.ApplicationTests;
import com.imageurforsynchrony.app.request.UserRequest;
import com.imageurforsynchrony.app.response.ApiResponse;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;

public class RegisterControllerTest extends ApplicationTests {

    private UserRequest userRequest = null;
    @Before
    public void setUp() {
        super.setUp();
        userRequest = new UserRequest("John","Doe");
    }

    /**
     * This method to test the register API
     * @throws Exception
     */
    @Test
    public void registerTest() throws Exception {
        String uri = "/register";
        String inputJson = super.mapToJson(userRequest);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);

        assertEquals(apiResponse.getMessage(), "User has been created successfully");
    }

}
