package com.imageurforsynchrony.app.controller;

import com.imageurforsynchrony.app.ApplicationTests;
import com.imageurforsynchrony.app.response.ApiResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public class UserControllerTest extends ApplicationTests {

    @Before
    public void setUp() {
        super.setUp();
    }

    /**
     * This method to test the getUsers API
     * @throws Exception
     */
    @Test
    public void getUsersTest() throws Exception {
        String uri = "/api/users";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        Assert.assertEquals("This method to test the users",apiResponse.getMessage(),"Users have to be created yet");
    }


}
