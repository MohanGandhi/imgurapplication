package com.imageurforsynchrony.app.utility;

import com.imageurforsynchrony.app.constants.ApplicationConstants;
import com.imageurforsynchrony.app.constants.FileExtensionAndSize;
import com.imageurforsynchrony.app.helper.ApiResponseUtility;
import com.imageurforsynchrony.app.helper.ImgUrResponseObject;
import com.imageurforsynchrony.app.response.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

public class FileValidation {

    /**
     * This method helps to check the file validation
     * @param file
     * @return
     */
    public static ApiResponse<ImgUrResponseObject> validate(MultipartFile file){
        if(file.isEmpty()){
            return ApiResponseUtility.generateApiResponseObject(HttpStatus.BAD_REQUEST.value(), ApplicationConstants.FILE_NOT_EMPTY,ApplicationConstants.UPLOAD_FAILED,ApplicationConstants.FAILED);
        }
        String[] fileName = StringUtils.split(file.getOriginalFilename(), ".");
        String fileExtn = fileName[fileName.length - 1];

        if(!FileExtensionAndSize.getFileExtnDetails().containsKey(fileExtn)){
            return ApiResponseUtility.generateApiResponseObject(HttpStatus.BAD_REQUEST.value(),ApplicationConstants.FILE_UNSUPPORTED,ApplicationConstants.BAD_FILE,ApplicationConstants.FAILED);
        }

        if(file.getSize() > FileExtensionAndSize.getFileExtnDetails().get(fileExtn)){
            return ApiResponseUtility.generateApiResponseObject(HttpStatus.BAD_REQUEST.value(),ApplicationConstants.FILE_SIZE_EXCEEDED_MESSAGE + +FileExtensionAndSize.getFileExtnDetails().get(fileExtn),ApplicationConstants.FILE_SIZE_EXCEEDED,ApplicationConstants.FAILED);
        }
        return null;
    }
}
