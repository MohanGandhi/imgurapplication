package com.imageurforsynchrony.app.utility;

import com.imageurforsynchrony.app.helper.ImgUrResponseObject;
import org.apache.commons.io.Charsets;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * This class helps to handle the response from IMGUR API
 */
public class CustomResponseHandler implements ResponseHandler {

    private HttpMethod httpMethod;

    public CustomResponseHandler(HttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    /**
     * This method converts the ImgUr response to our custom response object
     * @param response
     * @return
     * @throws IOException
     */
    @Override
    public ImgUrResponseObject handleResponse(final HttpResponse response) throws IOException {
        int status = response.getStatusLine().getStatusCode();
        ImgUrResponseObject responseObject = new ImgUrResponseObject();
        responseObject.setStatusCode(status);

        //If status is success then we are setting the values to our custom object.
        if (status >= 200 && status < 300) {
            HttpEntity entity = response.getEntity();
            Header headerEncoding = response.getEntity().getContentEncoding();

            Charset enocodedCharset = (headerEncoding == null) ? StandardCharsets.UTF_8 : Charsets.toCharset(headerEncoding.toString());
            String jsonResponse = EntityUtils.toString(entity, enocodedCharset);
            JSONObject jsonObject = new JSONObject(jsonResponse);
            if(HttpMethod.DELETE.name().equals(httpMethod.name())){
                responseObject.setSuccess((boolean)jsonObject.get("data"));
                return responseObject;
            }

            JSONObject dataObject = (JSONObject) jsonObject.get("data");
            responseObject.setLink((String) dataObject.get("link"));
            responseObject.setImageHash( HttpMethod.GET.name().equals(httpMethod.name()) ? "" : (String) dataObject.get("deletehash"));
            responseObject.setId((String) dataObject.get("id"));
            responseObject.setImgType((String) dataObject.get("type"));
            responseObject.setSuccess((boolean)jsonObject.get("success"));
        }

        return responseObject;
    }
}
