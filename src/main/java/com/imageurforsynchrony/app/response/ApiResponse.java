package com.imageurforsynchrony.app.response;

/**
 * This class uses to send the universal response
 * @param <T>
 */
public class ApiResponse<T> {
    private int code;
    private String status;
    private T result;
    private String error;
    private String message;

    public ApiResponse() {
    }

    public ApiResponse(int code, String status, T result, String error, String message) {
        this.code = code;
        this.status = status;
        this.result = result;
        this.error = error;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
