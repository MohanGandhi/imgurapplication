package com.imageurforsynchrony.app.repository;

import com.imageurforsynchrony.app.model.ImgUrResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImgUrRepository extends JpaRepository<ImgUrResponse, Long> {

        List<ImgUrResponse> findAllByUsername(String username);

        ImgUrResponse findByImgUrId(String imgUrId);

        @Query(" select img from ImgUrResponse img " +
                " where img.imageHash = ?1")
        ImgUrResponse findByImageHash(String imageHash);
}
