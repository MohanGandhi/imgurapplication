package com.imageurforsynchrony.app.constants;

import java.util.HashMap;
import java.util.Map;

public enum FileExtensionAndSize {
    ;

    private static final Map<String, Integer> fileExtnDetails = new HashMap<>();

    static {
        fileExtnDetails.put("png", 20971520);
        fileExtnDetails.put("gif", 209715200);
        fileExtnDetails.put("tif", 20971520);
        fileExtnDetails.put("jpg", 20971520);
        fileExtnDetails.put("bmp", 20971520);
    }


    public static Map<String, Integer> getFileExtnDetails() {
        return fileExtnDetails;
    }
}
