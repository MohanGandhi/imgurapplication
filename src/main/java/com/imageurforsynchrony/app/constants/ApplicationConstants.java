package com.imageurforsynchrony.app.constants;

public class ApplicationConstants {

    public static final String USER_CREATION_SUCCESS_MESSAGE = "User has been created successfully";
    public static final String USER_CREATION_FAILED_MESSAGE = "User creation failed";
    public static final String USER_ALREADY_EXIST = "User already exist";
    public static final String ISSUES_WITH_USER = "Issue with the user";
    public static final String EMPTY_USERS = "Users have to be created yet";
    public static final String SUCCESS = "Success";
    public static final String FAILED = "Failed";
    public static final String EMPTY = "";
    public static final String ALL_USERS = "List of users in the Application";

    // **************** File Upload constants Begin *****************
    public static final String AUTHORIZATION = "Authorization";
    public static final String CLIENT_ID = "Client-ID ";
    public static final String IMAGE = "image";
    public static final String NOT_FOUND = "Not Found";
    public static final String FILE_NOT_EMPTY = "File should not be empty";
    public static final String UPLOAD_FAILED = "Upload failed";
    public static final String FILE_UNSUPPORTED = "Image does not support to upload";
    public static final String BAD_FILE = "Image type does not support by ImgUR API";
    public static final String FILE_SIZE_EXCEEDED = "Image size exceeded";
    public static final String FILE_SIZE_EXCEEDED_MESSAGE = "Image size exceeded, please upload the given size : ";
    public static final String IMAGE_DELETED = "Image has been deleted successfully";
    public static final String IMAGE_DELETED_FAILED = "Failed to delte image from ImgUr API";
    public static final String IMAGE_NOT_FOUND = "Image does not exist";
    public static final String IMAGE_UPLOADED_SUCCESS = "Image has been uploaded successfully";
    //**********End***********
}
