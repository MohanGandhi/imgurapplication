package com.imageurforsynchrony.app.controller;

import com.imageurforsynchrony.app.constants.ApplicationConstants;
import com.imageurforsynchrony.app.model.User;
import com.imageurforsynchrony.app.request.UserRequest;
import com.imageurforsynchrony.app.response.ApiResponse;
import com.imageurforsynchrony.app.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class RegisterController {

    @Autowired
    private UserService userService;

    /**
     * This method to handle the request from front end save the details into DB
     * @param userRequest
     * @return
     */
    @PostMapping("/register")
    public ApiResponse<User> register(@Valid @RequestBody UserRequest userRequest){
        return userService.register(userRequest);
    }

    /**
     * This method handles the internal calls from security config once user is logged into the application
     * @return
     */
    @ApiOperation(value = "This methods calls from security config, should not expose the API to the world", hidden = true)
    @GetMapping("/loginSuccess")
    public ApiResponse<String> loginSuccess(){
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        return getApiResponse(name + " has been successfully logged in");
    }

    /**
     * This method handles the internal calls from security config once user is logged out the application
     * @return
     */
    @ApiOperation(value = "This methods calls from security config, should not expose the API to the world", hidden = true)
    @GetMapping("/logoutSuccess")
    public ApiResponse<String> logoutSuccess(){
        return getApiResponse( "User has been successfully logged out");
    }

    private ApiResponse<String> getApiResponse(String message){
        ApiResponse<String> apiResponse = new ApiResponse<>();
        apiResponse.setResult(message);
        apiResponse.setCode(HttpStatus.OK.value());
        apiResponse.setMessage(ApplicationConstants.SUCCESS);
        apiResponse.setError(ApplicationConstants.EMPTY);
        return apiResponse;
    }
}
