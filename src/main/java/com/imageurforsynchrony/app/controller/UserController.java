package com.imageurforsynchrony.app.controller;

import com.imageurforsynchrony.app.model.User;
import com.imageurforsynchrony.app.response.ApiResponse;
import com.imageurforsynchrony.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public ApiResponse<List<User>> getUsers(){
        return userService.getUsers();
    }


}
