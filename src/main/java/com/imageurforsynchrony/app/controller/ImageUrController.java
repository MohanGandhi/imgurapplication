package com.imageurforsynchrony.app.controller;

import com.imageurforsynchrony.app.constants.ApplicationConstants;
import com.imageurforsynchrony.app.helper.ApiResponseUtility;
import com.imageurforsynchrony.app.helper.ImgUrResponseObject;
import com.imageurforsynchrony.app.response.ApiResponse;
import com.imageurforsynchrony.app.service.ImageUploadService;
import com.imageurforsynchrony.app.utility.FileValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

import static com.imageurforsynchrony.app.helper.ApiResponseUtility.generateApiResponseObject;

/**
 * This class takes all the requests from client related to Image operations
 */
@RestController
@RequestMapping("/api/image")
public class ImageUrController {

    @Autowired
    private ImageUploadService imageUploadService;

    /**
     * This method helps to upload the image into IMGur API
     * @param file
     * @return
     * @throws IOException
     */
    @PostMapping("/upload")
    public ApiResponse<ImgUrResponseObject> uploadImage(@NotNull @RequestParam("file") MultipartFile file) throws IOException {

        ApiResponse<ImgUrResponseObject> imgUrResponseObjectApiResponse = FileValidation.validate(file);
        if(imgUrResponseObjectApiResponse != null){
            return imgUrResponseObjectApiResponse;
        }

        String encodedString = Base64.getEncoder().encodeToString(file.getBytes());
        ImgUrResponseObject imgUrResponseObject = imageUploadService.uploadImage(encodedString);

        ApiResponse apiResponse = generateApiResponseObject(HttpStatus.OK.value(), ApplicationConstants.IMAGE_UPLOADED_SUCCESS, null, ApplicationConstants.SUCCESS);
        apiResponse.setResult(imgUrResponseObject);
        return apiResponse;
    }

    /**
     * This method takes call from client to get the image by id from ImgUR API
     * @param imageId
     * @return
     */
    @GetMapping("/get/{imageId}")
    public ApiResponse<ImgUrResponseObject> getImage(@PathVariable("imageId") String imageId){
        ImgUrResponseObject imgUrResponseObject = imageUploadService.getImage(imageId);
        return generateApiResponseObject(imgUrResponseObject);
    }


    /**
     * This method takes call from client to get the image by id from Database
     * @param imageId
     * @return
     */
    @GetMapping("/getFromLocal/{imageId}")
    public ApiResponse<ImgUrResponseObject> getImageFromLocal(@PathVariable("imageId") String imageId){
        ImgUrResponseObject imgUrResponseObject = imageUploadService.getImageFromDB(imageId);
        return generateApiResponseObject(imgUrResponseObject);
    }

    /**
     * This method takes call from client to delete the image by imageHash
     * @param imageHash
     * @return
     */
    @DeleteMapping("/delete/{imageHash}")
    public ApiResponse<String> deleteImage(@PathVariable("imageHash") String imageHash){

        String result = imageUploadService.deleteImage(imageHash);
        ApiResponse<String> apiResponse = null;
        if(ApplicationConstants.SUCCESS.equals(result)){
            apiResponse = generateApiResponseObject(HttpStatus.OK.value(), ApplicationConstants.IMAGE_DELETED, null, ApplicationConstants.SUCCESS);
        }else{
            apiResponse = generateApiResponseObject(HttpStatus.BAD_REQUEST.value(), ApplicationConstants.IMAGE_DELETED_FAILED, ApplicationConstants.IMAGE_NOT_FOUND, ApplicationConstants.FAILED);
        }
        apiResponse.setResult(result);
        return apiResponse;
    }

    /**
     * This method to get all the images associated by the user
     * @param username
     * @return
     */
    @GetMapping("/images/all/{username}")
    public ApiResponse<List<ImgUrResponseObject>> findImagesByUsername(@PathVariable("username") String username){
            return imageUploadService.findImagesByUsername(username);
    }

}
