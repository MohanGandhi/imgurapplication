package com.imageurforsynchrony.app.service.impl;

import com.imageurforsynchrony.app.config.ImgURConfig;
import com.imageurforsynchrony.app.constants.ApplicationConstants;
import com.imageurforsynchrony.app.helper.ImgUrResponseObject;
import com.imageurforsynchrony.app.model.ImgUrResponse;
import com.imageurforsynchrony.app.repository.ImgUrRepository;
import com.imageurforsynchrony.app.response.ApiResponse;
import com.imageurforsynchrony.app.service.ImageUploadService;
import com.imageurforsynchrony.app.utility.CustomResponseHandler;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ImageUploadServiceImpl implements ImageUploadService {

    private static Logger LOGGER = LoggerFactory.getLogger(ImageUploadServiceImpl.class);

    @Autowired
    private ImgURConfig imgURConfig;

    @Autowired
    private ImgUrRepository imgUrRepository;

    /**
     * This method uses to upload the image into ImgUr API
     * @param base64String
     * @return
     */
    @Override
    public ImgUrResponseObject uploadImage(String base64String) {
        ImgUrResponseObject imgUrResponseObject = null;
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(imgURConfig.getBaseurl());
        httpPost.setHeader(ApplicationConstants.AUTHORIZATION, ApplicationConstants.CLIENT_ID + imgURConfig.getClientId());

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair(ApplicationConstants.IMAGE, base64String));

        CustomResponseHandler customResponseHandler = new CustomResponseHandler(HttpMethod.POST);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params));
            imgUrResponseObject = (ImgUrResponseObject) httpClient.execute(httpPost, customResponseHandler);
            if(imgUrResponseObject.getStatusCode() >= 200){
                LOGGER.info("Image has been successfully uploaded into ImgUr API");
                saveRecordsIntoDB(imgUrResponseObject);
            }
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Image content does not supported into ImgUr API",e.getMessage());
        } catch (ClientProtocolException e) {
            LOGGER.error("Iusse while connecting to ImgUr API",e.getMessage());
        } catch (IOException e) {
            LOGGER.error("Iusse while uploading image into to ImgUr API",e.getMessage());
        }
        return imgUrResponseObject;
    }

    /**
     * This method uses to save the ImgUr response into our database for reference
     * @param imgUrResponseObject
     */
    private void saveRecordsIntoDB(ImgUrResponseObject imgUrResponseObject) {
        Authentication auth
                = SecurityContextHolder.getContext().getAuthentication();
        ImgUrResponse imgUrResponse = new ImgUrResponse(imgUrResponseObject.getId(),imgUrResponseObject.getLink(),imgUrResponseObject.getImgType(),imgUrResponseObject.getImageHash(), auth.getName());
        imgUrRepository.save(imgUrResponse);
        LOGGER.info("Image has been successfully saved into Database");
    }

    /**
     * This method uses to get image from IMGUR API
     * @param imageId
     * @return
     */
    @Override
    public ImgUrResponseObject getImage(String imageId) {
        ImgUrResponseObject imgUrResponseObject = null;
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(imgURConfig.getBaseurl() + "/" + imageId);
        httpGet.setHeader(ApplicationConstants.AUTHORIZATION, ApplicationConstants.CLIENT_ID + imgURConfig.getClientId());
        CustomResponseHandler customResponseHandler = new CustomResponseHandler(HttpMethod.GET);
        try {
            imgUrResponseObject = (ImgUrResponseObject)httpClient.execute(httpGet, customResponseHandler);
            LOGGER.info("image has been retrieved from ImgUR API");
        } catch (IOException e) {
            LOGGER.error("Exception occurred while getting image from ImgUR API");
           return imgUrResponseObject;
        }

        return imgUrResponseObject;
    }

    /**
     * This method uses to get images from DB. We save the records into local DB, so nwe can get the details from our DB. we can reduce the network traffic
     * @param imageId
     * @return
     */
    @Override
    public ImgUrResponseObject getImageFromDB(String imageId) {
        ImgUrResponseObject imgUrResponseObject = null;
        try {
            ImgUrResponse imgUrResponse = imgUrRepository.findByImgUrId(imageId);
            LOGGER.info("image has been retrieved from our DB");
            imgUrResponseObject = convertObject(imgUrResponse);
        } catch (Exception e) {
            LOGGER.error("exception occured has been retrieved from out DB");
            return imgUrResponseObject;
        }

        return imgUrResponseObject;
    }

    /**
     * This method uses to delete the image by imageHash from API.
     * @param imageId
     * @return
     */
    @Override
    public String deleteImage(String imageId) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpDelete httpDelete = new HttpDelete(imgURConfig.getBaseurl() + "/" + imageId);
        httpDelete.setHeader(ApplicationConstants.AUTHORIZATION, ApplicationConstants.CLIENT_ID + imgURConfig.getClientId());
        CustomResponseHandler customResponseHandler = new CustomResponseHandler(HttpMethod.DELETE);
        try {
            ImgUrResponseObject imgUrResponseObject = (ImgUrResponseObject)httpClient.execute(httpDelete, customResponseHandler);
            return imgUrResponseObject.getStatusCode() >= 200 && imgUrResponseObject.getStatusCode() < 300 ? deleteImageFromDB(imageId) : ApplicationConstants.FAILED;
        } catch (IOException e) {
            return "Exception occurred while deleting image from ImgUr API";
        }
    }

    /**
     * This method deletes the data from our DB
     * @param imageHash
     * @return
     */
    private String deleteImageFromDB(String imageHash) {
        ImgUrResponse imgUrResponse = imgUrRepository.findByImageHash(imageHash);
        if(imgUrResponse != null) {
            imgUrRepository.delete(imgUrResponse);
            return ApplicationConstants.SUCCESS;
        }else{
            return "Unable to delete an image from Database";
        }
    }

    /**
     * This method retrieves the list of images from Database by given username
     * @param username
     * @return
     */
    @Override
    public ApiResponse<List<ImgUrResponseObject>> findImagesByUsername(String username) {
        List<ImgUrResponse> imgUrResponses = imgUrRepository.findAllByUsername(username);
        ApiResponse<List<ImgUrResponseObject>> apiResponse = new ApiResponse<>();
        List<ImgUrResponseObject>  imgUrResponseObjects = null;
        if(!CollectionUtils.isEmpty(imgUrResponses)){
             imgUrResponseObjects = convertListOfObject(imgUrResponses);
            apiResponse.setMessage("List of images associated by the " + username);
            apiResponse.setCode(HttpStatus.OK.value());
            apiResponse.setStatus(ApplicationConstants.SUCCESS);
        }else{
            apiResponse.setCode(HttpStatus.BAD_REQUEST.value());
            apiResponse.setMessage("No users has been found by the "+username);
            apiResponse.setStatus(ApplicationConstants.FAILED);
        }
        apiResponse.setResult(imgUrResponseObjects);
        return apiResponse;
    }

    /**
     * This method uses to convert the list of entity object into DTO
     * @param imgUrResponses
     * @return
     */
    private  List<ImgUrResponseObject> convertListOfObject(List<ImgUrResponse> imgUrResponses) {
        List<ImgUrResponseObject> responseObjects = null;
        if(!CollectionUtils.isEmpty(imgUrResponses)){
            responseObjects = new ArrayList<>();
            for(ImgUrResponse imgUrResponse : imgUrResponses){
                responseObjects.add(convertObject(imgUrResponse));
            }
        }
        return responseObjects;
    }

    /**
     * This method uses to convert the entity object into DTO
     * @param imgUrResponse
     * @return
     */
    private ImgUrResponseObject convertObject(ImgUrResponse imgUrResponse){
        ImgUrResponseObject imgUrResponseObject = null;
        if(imgUrResponse != null){
            imgUrResponseObject = new ImgUrResponseObject(imgUrResponse.getImgUrLink(),imgUrResponse.getImgUrId(),imgUrResponse.getImgType(),imgUrResponse.getImageHash(), 200,true);
        }
        return imgUrResponseObject;
    }
}
