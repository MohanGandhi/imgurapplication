package com.imageurforsynchrony.app.service.impl;

import com.imageurforsynchrony.app.constants.ApplicationConstants;
import com.imageurforsynchrony.app.model.User;
import com.imageurforsynchrony.app.repository.UserRepository;
import com.imageurforsynchrony.app.request.UserRequest;
import com.imageurforsynchrony.app.response.ApiResponse;
import com.imageurforsynchrony.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;

import static com.imageurforsynchrony.app.constants.ApplicationConstants.*;

/**
 * This class is a business layer which called from controller
 * and the call to repository to get the data from Database
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    /**
     * This method to checks whether user is exist or not by username
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Objects.requireNonNull(username);
        User user = userRepository.findUserWithName(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        return user;
    }

    /**
     * This method to checks whether user is exist or not by username and password
     * @param username
     * @param password
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsernameAndPassword(String username, String password) throws UsernameNotFoundException {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);
//        String encodedPwd = passwordEncoder.encode(password);
        User user =  userRepository.findUserWithNameAndPassword(username, password);
        return user;
    }

    /**
     * This method uses to register the user
     * @param userRequest
     * @return
     */
    @Override
    public ApiResponse<User> register(UserRequest userRequest) {
        //To check whether the username is exists in the DB or not
        User existedUser = userRepository.findByUsername(userRequest.getUsername());
        ApiResponse<User> apiResponse = new ApiResponse<>();
        if(Objects.isNull(existedUser)) {
//            User user = new User(userRequest.getUsername(), passwordEncoder.encode(userRequest.getPassword()));
            User user = new User(userRequest.getUsername(), userRequest.getPassword());
            User savedUser = userRepository.save(user);
            if(savedUser.getId() > 0){
                apiResponse.setResult(savedUser);
                successApiResponse(apiResponse, ApplicationConstants.USER_CREATION_SUCCESS_MESSAGE, HttpStatus.CREATED.value());
            }else{
                failedApiResponse(apiResponse,ISSUES_WITH_USER,ApplicationConstants.USER_CREATION_FAILED_MESSAGE, HttpStatus.BAD_REQUEST.value());
            }
        }else{
            apiResponse.setCode(HttpStatus.BAD_REQUEST.value());
            failedApiResponse(apiResponse,USER_ALREADY_EXIST, FAILED,HttpStatus.BAD_REQUEST.value());
        }

        return apiResponse;
    }

    /**
     * This method uses to get the list of all users
     * @return
     */
    @Override
    public ApiResponse<List<User>> getUsers() {
        List<User> users = userRepository.findAll();
        ApiResponse<List<User>> apiResponse = new ApiResponse<>();
        if(CollectionUtils.isEmpty(users)){
            failedApiResponse(apiResponse,EMPTY_USERS,FAILED, HttpStatus.NOT_FOUND.value());
        }else{
            apiResponse.setResult(users);
            successApiResponse(apiResponse,ApplicationConstants.ALL_USERS, HttpStatus.OK.value());
        }
        return apiResponse;
    }

    /**
     * This method uses to set the failed response details to api response object
     * @param apiResponse
     */
    private void failedApiResponse(ApiResponse apiResponse, String message, String error, int code){
        apiResponse.setCode(code);
        apiResponse.setError(error);
        apiResponse.setStatus(ApplicationConstants.FAILED);
        apiResponse.setMessage(message);
    }

    /**
     * This method uses to set the success response details to api response object
     * @param apiResponse
     */
    private void successApiResponse(ApiResponse apiResponse, String message, int code){
        apiResponse.setStatus(ApplicationConstants.SUCCESS);
        apiResponse.setError(ApplicationConstants.EMPTY);
        apiResponse.setMessage(message);
        apiResponse.setCode(code);
    }

}
