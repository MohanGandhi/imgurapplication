package com.imageurforsynchrony.app.service;

import com.imageurforsynchrony.app.model.User;
import com.imageurforsynchrony.app.request.UserRequest;
import com.imageurforsynchrony.app.response.ApiResponse;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

public interface UserService  extends UserDetailsService {

    ApiResponse<User> register(UserRequest userRequest);

    ApiResponse<List<User>> getUsers();

    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

    UserDetails loadUserByUsernameAndPassword(String username, String password) throws UsernameNotFoundException;
}
