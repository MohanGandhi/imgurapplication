package com.imageurforsynchrony.app.service;

import com.imageurforsynchrony.app.helper.ImgUrResponseObject;
import com.imageurforsynchrony.app.response.ApiResponse;

import java.util.List;

public interface ImageUploadService {

    ImgUrResponseObject uploadImage(String imageLink);

    ImgUrResponseObject getImage(String imageLink);

    ImgUrResponseObject getImageFromDB(String imageLink);

    String deleteImage(String imageLink);

    ApiResponse<List<ImgUrResponseObject>> findImagesByUsername(String username);
}
