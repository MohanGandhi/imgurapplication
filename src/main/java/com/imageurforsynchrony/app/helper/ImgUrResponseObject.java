package com.imageurforsynchrony.app.helper;

public class ImgUrResponseObject {
    private String link;
    private String id;
    private String imgType;
    private String imageHash;
    private int statusCode;
    private boolean isSuccess;

    public ImgUrResponseObject() {
    }

    public ImgUrResponseObject(String link, String id, String imgType, String imageHash, int statusCode, boolean isSuccess) {
        this.link = link;
        this.id = id;
        this.imgType = imgType;
        this.imageHash = imageHash;
        this.statusCode = statusCode;
        this.isSuccess = isSuccess;
    }

    public String getImageHash() {
        return imageHash;
    }

    public void setImageHash(String imageHash) {
        this.imageHash = imageHash;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getImgType() {
        return imgType;
    }

    public void setImgType(String imgType) {
        this.imgType = imgType;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    @Override
    public String toString() {
        if (statusCode >= 200 && statusCode < 300) {
            return "ImgUrResponseObject{" +
                    "link='" + link + '\'' +
                    ", id='" + id + '\'' +
                    ", imgType='" + imgType + '\'' +
                    ", status='" + statusCode + '\'' +
                    ", isSuccess=" + isSuccess +
                    '}';
        } else {
            return "ImgUrResponseObject{" +
                    ", status='" + statusCode + '\'' +
                    '}';
        }
    }
}
