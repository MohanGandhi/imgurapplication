package com.imageurforsynchrony.app.helper;

import com.imageurforsynchrony.app.constants.ApplicationConstants;
import com.imageurforsynchrony.app.response.ApiResponse;
import org.springframework.http.HttpStatus;

/**
 * This class helps to convert the response object into ApiResponse object
 */
public class ApiResponseUtility<T> {

    /**
     * This method generate API response object
     * @param imgUrResponseObject
     * @return
     */
    public static ApiResponse<ImgUrResponseObject> generateApiResponseObject(ImgUrResponseObject imgUrResponseObject){
        ApiResponse<ImgUrResponseObject> apiResponse = new ApiResponse<>();
        if(imgUrResponseObject != null){
            apiResponse.setResult(imgUrResponseObject);
            if(imgUrResponseObject.getStatusCode() >= 200 && imgUrResponseObject.getStatusCode() < 300){
                apiResponse.setStatus(ApplicationConstants.SUCCESS);
            }else{
                apiResponse.setStatus(ApplicationConstants.NOT_FOUND);
            }
            apiResponse.setCode(imgUrResponseObject.getStatusCode());

        }else{
            apiResponse.setResult(imgUrResponseObject);
            apiResponse.setCode(HttpStatus.BAD_REQUEST.value());
            apiResponse.setMessage("Id is invalid");
            apiResponse.setStatus(ApplicationConstants.FAILED);
            apiResponse.setError("Invalid Id");
        }
        return apiResponse;
    }

    public static ApiResponse generateApiResponseObject(int code, String message, String error, String status){
        ApiResponse apiResponse = new ApiResponse<>();
        apiResponse.setCode(code);
        apiResponse.setMessage(message);
        apiResponse.setStatus(status);
        apiResponse.setError(error);
        return apiResponse;
    }
}
