package com.imageurforsynchrony.app.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ImgURConfig {

   @Value("${imgur.clientId}")
    private String clientId;

   @Value("${imgur.baseurl}")
    private String baseurl;

    public String getClientId() {
        return clientId;
    }

    public String getBaseurl() {
        return baseurl;
    }
}
