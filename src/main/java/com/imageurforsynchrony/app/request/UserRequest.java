package com.imageurforsynchrony.app.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * This class helps as a request class to bind the values from client
 */
public class UserRequest {

    @NotBlank(message = "username is mandatory")
    @Size(min = 3,max = 50)
    private String username;

    @NotBlank(message = "password is mandatory")
    @Size(min = 3,max = 50)
    private String password;

    public UserRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
