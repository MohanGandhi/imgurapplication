package com.imageurforsynchrony.app.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ImgUrResponse implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "imgur_id")
    private String imgUrId;

    @Column(name = "imgur_link")
    private String imgUrLink;

    @Column(name = "imgur_Type")
    private String imgType;

    @Column(name = "image_hash")
    private String imageHash;
/*
    @Column(name = "status_code")
    private String username;*/

    @Column(name = "username")
    private String username;

    public ImgUrResponse() {
    }

    public ImgUrResponse(String imgUrId, String imgUrLink, String imgType, String imageHash, String username) {
        this.imgUrId = imgUrId;
        this.imgUrLink = imgUrLink;
        this.imgType = imgType;
        this.imageHash = imageHash;
        this.username = username;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImgUrId() {
        return imgUrId;
    }

    public void setImgUrId(String imgUrId) {
        this.imgUrId = imgUrId;
    }

    public String getImgUrLink() {
        return imgUrLink;
    }

    public void setImgUrLink(String imgUrLink) {
        this.imgUrLink = imgUrLink;
    }

    public String getImgType() {
        return imgType;
    }

    public void setImgType(String imgType) {
        this.imgType = imgType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImageHash() {
        return imageHash;
    }

    public void setImageHash(String imageHash) {
        this.imageHash = imageHash;
    }
}
