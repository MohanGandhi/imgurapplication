Step 1 : Open the link http://localhost:8070/swagger-ui.html#/ to easily access the APIs

Step 2 : On the page, you can see register-controller, there you have to register the basic details like username and pwd

Step 3 : Please open the Auth API, then enter the registered details to access the rest of the APIs

step 4 :  Once you logged in, please open the Image UR Controller API to upload the images

step 5 : TO delete the image, make sure to provide imageHash not image ID

Note : http://localhost:8070/h2 to access the H2 Database